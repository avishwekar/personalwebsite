#FROM is the base image for which we will run our application
FROM nginx:latest

# Copy files and directories from the application
COPY index.html /usr/share/nginx/html
COPY books.html /usr/share/nginx/html
COPY carousaltest.html /usr/share/nginx/html
COPY config.rb /usr/share/nginx/html
COPY contactform.htm /usr/share/nginx/html
COPY data.json /usr/share/nginx/html
COPY game.html /usr/share/nginx/html
COPY test.html /usr/share/nginx/html
COPY send_form_email.php /usr/share/nginx/html
COPY css/ /usr/share/nginx/html/css/
COPY images/ /usr/share/nginx/html/
COPY js/ /usr/share/nginx/html/js/
COPY sample/ /usr/share/nginx/html/sample/
COPY src/ /usr/share/nginx/html/src/